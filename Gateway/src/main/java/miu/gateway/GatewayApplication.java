package miu.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}


//	@Bean
//	public RouteLocator myRoutes(RouteLocatorBuilder builder) {
//		return builder.routes()
//				.route("path_route", r -> r.path("/get")
//						.uri("http://httpbin.org"))
//				.route(p -> p
//					.path("/product")
//					.uri("http://localhost:8091"))
//				.route(p -> p
//						.path("/shopping")
//						.uri("http://localhost:8093"))
//				.route(p -> p
//						.path("/order")
//						.uri("http://localhost:8094"))
//				.route("customer", p -> p
//						.path("/customer")
//						.uri("http://localhost:8092/customer/101"))
//				.route("default", p -> p
//						.path("/default")
//						.uri("http://httpbin.org:80"))
//				.build();
//	}

	@RequestMapping("/error")
	public Mono<String> fallback() {
		return Mono.just("fallback");
	}



}
