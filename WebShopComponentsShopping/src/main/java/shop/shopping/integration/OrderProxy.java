package shop.shopping.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import org.springframework.web.client.RestTemplate;
import shop.shopping.service.ShoppingCartDTO;

@Component
public class OrderProxy {
	@Autowired
	private RestOperations restTemplate;
	@Value("${ordersURL}")
	String ordersURL;

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return restTemplate;
	}

	public void createOrder(ShoppingCartDTO shoppingCartDTO) {		
		restTemplate.postForLocation(ordersURL+"/order/create", shoppingCartDTO);		
	};
}
