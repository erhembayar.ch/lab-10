package miu.serviceB;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class ServiceBApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceBApplication.class, args);
	}
	private static Logger log = LoggerFactory.getLogger(ServiceBApplication.class);

	@Autowired
	private CircuitBreakerFactory circuitBreakerFactory;

	@GetMapping("/get")
	public String get(){
		log.info("Service B ");
		RestTemplate restTemplate = new RestTemplate();
		CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");

		String c = circuitBreaker.run(() ->
				restTemplate.getForObject("http://localhost:8073/get", String.class),
				throwable -> getFallBack());
		return "from Service B,  " + c;
	}

	public String getFallBack(){
		return " , -- fallback -- ";
	}
}
