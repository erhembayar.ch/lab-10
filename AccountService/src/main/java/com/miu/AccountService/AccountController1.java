package com.miu.AccountService;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("One")
public class AccountController1 {

    @GetMapping("/account/{customerid}")
	public String getName(@PathVariable("customerid") String customerId){
		return "Account 1: " + customerId;

	}
}
