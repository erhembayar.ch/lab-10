package shop.order.integration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import org.springframework.web.client.RestTemplate;
import shop.order.service.OrderCustomerDTO;



@Component
public class CustomerProxy {
	@Autowired
	private RestTemplate restTemplate;

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return restTemplate;
	}

	@Value("${customersURL}")
	String customersURL;
	
	public OrderCustomerDTO getOrderCustomer(String customerNumber) {
		OrderCustomerDTO customer = restTemplate.getForObject("http://customer/ordercustomer/"+customerNumber, OrderCustomerDTO.class);
		return customer;
	};
}



