package shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;



@SpringBootApplication
//@EnableDiscoveryClient
@RestController
public class WebShopApplication implements CommandLineRunner {
	@Autowired
	private RestTemplate  restTemplate;

	public static void main(String[] args) {
		SpringApplication.run(WebShopApplication.class, args);
	}

//	@Bean
//	@LoadBalanced
//	public RestTemplate restTemplate() {
//		return new RestTemplate();
//	}

	@Bean
//	@LoadBalanced
	RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return restTemplate;
	}

	@Override
	public void run(String... args) throws Exception {

		
	}

	@GetMapping("/run")
	public void getRun(){
		//create customer
		CustomerDTO customerDto = new CustomerDTO("101","Frank","Brown","fBrown@Hotmail.com","123456");
		AddressDTO addressDTO = new AddressDTO("1000 N main Street", "Fairfield","52557","USA");
		customerDto.setAddress(addressDTO);

		String gatewayUrl = "http://localhost:8099";

		restTemplate.postForLocation(gatewayUrl + "/customer", customerDto);
		// get customer
		System.out.println("\n-----Customer-------");
		CustomerDTO customerDtoGet = restTemplate.getForObject(gatewayUrl+ "/customer/101", CustomerDTO.class);
		System.out.println(customerDtoGet);

		//create products
		restTemplate.postForLocation(gatewayUrl+ "/product/A33/TV/250.00", null);
		restTemplate.postForLocation(gatewayUrl+ "/product/A34/MP3/75.00", null);
		//set stock
		restTemplate.postForLocation(gatewayUrl+ "/product/stock/A33/433/A557", null);
		restTemplate.postForLocation(gatewayUrl+ "/product/stock/A34/250/A557", null);
		//get a product
		ProductDTO product = restTemplate.getForObject(gatewayUrl+ "/product/A33", ProductDTO.class);
		System.out.println("\n-----Product-------");
		product.print();
		// add products to the shoppingcart
		restTemplate.postForLocation(gatewayUrl+ "/shopping/1/A33/3", null);
		restTemplate.postForLocation(gatewayUrl+ "/shopping/1/A34/2", null);
		//get the shoppingcart
		ShoppingCartDTO cart = restTemplate.getForObject(gatewayUrl+ "/shopping/1", ShoppingCartDTO.class);
		System.out.println("\n-----Shoppingcart-------");
		if (cart != null) cart.print();
		//checkout the cart
		restTemplate.postForLocation(gatewayUrl+ "/shopping/checkout/1", null);
		//get the order
		OrderDTO order = restTemplate.getForObject(gatewayUrl+ "/order/1", OrderDTO.class);
		System.out.println("\n-----Order-------");
		if (order != null) order.print();

		//add customer to order
		restTemplate.postForLocation(gatewayUrl+ "/order/setCustomer/1/101", null);

		//confirm the order
		restTemplate.postForLocation(gatewayUrl+ "/order/1", null);

		//get the order
		OrderDTO orderconfirmed = restTemplate.getForObject(gatewayUrl+ "/order/1", OrderDTO.class);
		System.out.println("\n-----Confirmed Order-------");
		if (orderconfirmed != null) orderconfirmed.print();
	}

}
