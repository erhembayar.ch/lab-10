package miu.serviceA;

import brave.sampler.Sampler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.Span;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class ServiceAApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceAApplication.class, args);
	}
	private static Logger log = LoggerFactory.getLogger(ServiceAApplication.class);

	@GetMapping("/get")
	public String get(){
		log.info("Service A");

		RestTemplate restTemplate = new RestTemplate();

		String b = restTemplate.getForObject("http://localhost:8072/get", String.class);
		return "from Service A,  " + b;
	}

	@Bean
	Sampler customSampler() {
		return new Sampler() {
			@Override
			public boolean isSampled(long l) {
				return Math.random() > .5 ;
			}
		};
	}

}
